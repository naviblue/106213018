<?php
session_start();
require "userModel.php";
//check whether the user has logged in or not
if (! isSet($_SESSION["loginProfile"])) {
    //if not logged in, redirect page to loginUI.php
    header("Location: loginUI.php");
}
require "orderModel.php";
$address=$_POST['address'];
$oak = getUserOak($_SESSION["loginProfile"]['uID']);

if ($oak >= 5) {
    echo "You are oak, can't shop!";
} elseif ($oak <= 4) {
    checkout($_SESSION["loginProfile"]["uID"], $address);
    echo "感謝! 訂單處理中...";
} else {
    echo "sorry, internal error, please try again..";
}

header("refresh:1; url = main.php");
?>
<!-- <a href="main.php">OK</a> -->
