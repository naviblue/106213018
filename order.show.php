<?php
session_start();
require "orderModel.php";
require "userModel.php";
//check whether the user has logged in or not
if (! isSet($_SESSION["loginProfile"])) {
    //if not logged in, redirect page to loginUI.php
    header("Location: loginUI.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; margin: auto; }
    </style>
</head>
<body>
<div class="wrapper">
<p>This is the list of you historical orders</p>
<hr>
<?php
$oak = getUserOak($_SESSION["loginProfile"]["uID"]);
    echo "Hello ", $_SESSION["loginProfile"]["uName"],
    ", Your ID is: ", $_SESSION["loginProfile"]["uID"],
    ", Your Role is: ", $_SESSION["loginProfile"]["uRole"],
    ", Your Oak grade is: ", $oak,"<HR>";

    $result=getOrderList($_SESSION["loginProfile"]["uID"]);
?>
    <table width="200" border="1">
  <tr>
    <td>order ID</td>
    <td>Date</td>
    <td>Status</td>
    <td>address</td>
    <td>Detail</td>
    <td></td>
  </tr>
<?php
while (    $rs=mysqli_fetch_assoc($result)) {
    echo "<tr><td>" . $rs['ordID'] . "</td>";
    echo "<td>{$rs['orderDate']}</td>";
    echo "<td>{$rs['status']}</td>";
    echo "<td>{$rs['address']}</td>";
    echo "<td><a href='order.showDetail.php?ID=" , $rs['ordID'] , "'>ShowDetail</a></td>";
    if (1 <= $rs['status'] && $rs['status'] < 3) {
        echo "<td><a href='cancel.php?id=" , $rs['ordID'] , "'>Cancel</a></td>";
    } else {
        echo "<td>";
    }
}
?>
</table>
<a href="main.php">OK</a><hr>
</div>
</body>
</html>
