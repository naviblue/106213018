<?php
session_start();
//check whether the user has logged in or not
if (! isSet($_SESSION["loginProfile"])) {
    //if not logged in, redirect page to loginUI.php
    header("Location: loginUI.php");
}
require "orderModel.php";
$ordID=(int)$_GET['id'];

if (takeout($ordID)) {
    echo "訂單已出貨...";
} else {
    echo "sorry, internal error, please try again..";
}
header("refresh:1; url = porter.php");
?>
<!-- <a href="admin.php">OK</a> -->
