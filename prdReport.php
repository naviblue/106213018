<?php
session_start();
require "prdModel.php";

//check whether the user has logged in or not
if (! isset($_SESSION["loginProfile"])) {
    //if not logged in, redirect page to loginUI.php
    header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; margin: auto; }
    </style>
</head>
<body>
<div class="wrapper">
<h2>This is the Report page</h2>
[<a href="logout.php">logout</a>] [<a href="admin.php">Admin Main Page</a>]

</p>
<hr>
<?php
    echo "Hello ", $_SESSION["loginProfile"]["uName"],
    ", Your ID is: ", $_SESSION["loginProfile"]["uID"],
    ", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>"
    ;
?>
<hr>
Sales
<table width="350px" border="1">
  <tr>
    <!-- <td>pid</td> -->
    <td>name</td>
    <td>sales</td>
    <!-- <td></td> -->
  </tr>
<?php
$result=getPrdReportList();
while ($rs=mysqli_fetch_assoc($result)) {
    // echo "<tr><td>" . $rs['prdID'] . "</td>";
    echo "<tr><td>{$rs['name']}</td>";
    echo "<td>{$rs['sales']}</td>";
}
?>
</table>

<hr>
VIP
<table width="350px" border="1">
  <tr>
    <td>name</td>
    <td>total amount</td>
  </tr>
<?php
$result=getVIPList();
while ($rs=mysqli_fetch_assoc($result)) {
    echo "<tr><td>{$rs['name']}</td>";
    echo "<td>{$rs['total']}</td>";
}
?>
</table>
</div>
</body>
</html>
