-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.6-MariaDB
-- PHP 版本： 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `1081se`
--

-- --------------------------------------------------------

--
-- 資料表結構 `orderitem`
--

CREATE TABLE `orderitem` (
  `serno` int(11) NOT NULL,
  `ordID` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `orderitem`
--

INSERT INTO `orderitem` (`serno`, `ordID`, `prdID`, `quantity`) VALUES
(73, 21, 1, 1),
(74, 21, 1, 1),
(75, 24, 1, 1),
(76, 24, 1, 1),
(77, 24, 1, 1),
(78, 24, 1, 1),
(79, 24, 1, 1),
(80, 24, 1, 1),
(81, 24, 1, 1),
(82, 25, 2, 1),
(83, 25, 2, 1),
(84, 25, 1, 1),
(85, 26, 2, 1),
(86, 26, 2, 1),
(87, 26, 3, 1),
(88, 26, 1, 1),
(110, 27, 1, 1),
(111, 27, 2, 1),
(112, 27, 7, 1),
(113, 27, 3, 1),
(114, 27, 2, 1),
(115, 28, 3, 1),
(116, 29, 3, 1),
(123, 30, 1, 1),
(124, 30, 1, 1),
(125, 30, 1, 1),
(126, 30, 1, 1),
(127, 30, 2, 1),
(129, 31, 8, 1),
(130, 31, 7, 1),
(131, 31, 2, 1),
(132, 31, 1, 1),
(137, 32, 2, 1),
(138, 32, 1, 1),
(139, 32, 3, 1),
(140, 32, 8, 1),
(141, 32, 1, 1),
(146, 31, 2, 1),
(147, 31, 1, 1),
(148, 31, 2, 1),
(149, 35, 1, 1),
(150, 35, 3, 1),
(151, 35, 1, 1),
(152, 36, 2, 1),
(153, 37, 1, 1),
(154, 38, 2, 1),
(155, 39, 1, 1),
(156, 36, 1, 1),
(157, 33, 2, 1),
(158, 40, 1, 1),
(159, 33, 1, 1),
(160, 33, 1, 1),
(161, 21, 7, 1),
(162, 22, 1, 1),
(163, 23, 7, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `prdID` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `detail` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `product`
--

INSERT INTO `product` (`prdID`, `name`, `price`, `detail`) VALUES
(1, 'iPhone 15', 90000, 'This is a fake phone. Don\'t bu'),
(2, 'Water', 90, 'Pure water from Puli'),
(3, 'Air', 0, 'PM 2.5 air for free'),
(7, '108SE', 0, 'fwef'),
(8, 'aaa', 45, 'fwef');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `oak` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`ID`, `password`, `name`, `role`, `oak`) VALUES
('1', '1', '1', 1, 5),
('777', '777', '777', 1, 9),
('admin', '123', '管理員', 9, 0),
('porter', '123', '物流人員', 8, 0),
('user', '123', '客戶', 1, 8);

-- --------------------------------------------------------

--
-- 資料表結構 `userorder`
--

CREATE TABLE `userorder` (
  `ordID` int(11) NOT NULL,
  `uID` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orderDate` date NOT NULL,
  `amount` int(11) NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `userorder`
--

INSERT INTO `userorder` (`ordID`, `uID`, `orderDate`, `amount`, `address`, `status`) VALUES
(21, 'user', '2020-01-08', 0, '埔里', 0),
(22, 'user', '2020-01-08', 0, '埔里', 0),
(23, 'user', '2020-01-07', 0, '', 0),
(24, 'user', '2020-01-08', 0, '', 0),
(25, 'user', '2020-01-08', 0, '埔里', 0),
(26, 'user', '2020-01-08', 0, '', 0),
(27, 'user', '2020-01-08', 0, '123', 3),
(28, 'user', '2020-01-08', 0, '埔里', 0),
(29, 'user', '2020-01-08', 0, '埔里', 2),
(30, 'user', '2020-01-08', 0, '埔里', 0),
(31, 'user', '2020-01-08', 0, '埔里', 2),
(32, '1', '2020-01-08', 0, '埔里', 3),
(33, '1', '2020-01-08', 0, '埔里', 0),
(34, 'user', '0000-00-00', 0, '', 0),
(35, '777', '2020-01-08', 0, '埔里', 1),
(36, '777', '2020-01-08', 0, '埔里', 0),
(37, '777', '2020-01-08', 0, '123', 0),
(38, '777', '2020-01-08', 0, '埔里', 0),
(39, '777', '2020-01-08', 0, '埔里', 0),
(40, '1', '2020-01-08', 0, '埔里', 0);

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prdID`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `userorder`
--
ALTER TABLE `userorder`
  ADD PRIMARY KEY (`ordID`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `product`
--
ALTER TABLE `product`
  MODIFY `prdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `userorder`
--
ALTER TABLE `userorder`
  MODIFY `ordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
